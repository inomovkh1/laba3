﻿using System;

namespace Laba3TRPO.ClassLibary
{
    public class Class1
    {
        public static double Func(double a, double n)
        {
            double s = (a * a * n) / (4 * Math.Tan(180/n/180*Math.PI));
            return s;
        }
    }
}

