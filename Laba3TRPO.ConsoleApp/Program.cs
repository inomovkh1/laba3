﻿using System;
namespace Laba3TRPO.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите длину сторон многоугольника (a): ");
            double a =Convert.ToDouble( Console.ReadLine());
            Console.Write("Введите количество сторон многоугольника (n): ");
            double n = Convert.ToDouble(Console.ReadLine());
            double s = Laba3TRPO.ClassLibary.Class1.Func(a, n);
            Console.Write("Площадь правильного многоугольника: ");
            Console.Write(s);
        }
    }
}
